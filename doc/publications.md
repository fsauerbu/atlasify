# Used in

This list summarizes articles and publications using the atlasify package. The
list is incomplete.

 - F. Sauerburger, *H→ττ cross section measurements using machine learning and
   its interpretation as global couplings with the ATLAS detector*,
   [10.6094/UNIFR/230080](https://doi.org/10.6094/UNIFR/230080)
 - ATLAS Collaboration, *Measurements of Higgs boson production cross-sections in
   the 𝐻 → 𝜏+𝜏− decay channel in 𝑝𝑝 collisions at √𝑠 = 13 TeV with the ATLAS
   detector*, [JHEP **08** (2022) 175](https://link.springer.com/article/10.1007/JHEP08(2022)175) 
 - F. Sauerburger, ATLAS Collaboration, *Measurement of the Higgs boson coupling
   to 𝜏-leptons in proton-proton collisions at √𝑠 = 13 TeV with the ATLAS
   detector at the LHC*, [PoS **EPS-HEP2021** (2022) 573](https://pos.sissa.it/398/573)
 - ATLAS Collaboration, *Combined measurements of Higgs boson production and
   decay using up to 139 fb−1 of proton-proton collision data at √𝑠 = 13 TeV
   collected with the ATLAS experiment*, [ATLAS-CONF-2021-053](https://cds.cern.ch/record/2789544)
 - The [uhepp](https://uhepp.org) reference implementation is based on atlasify
