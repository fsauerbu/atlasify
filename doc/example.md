# Example
*Real* world example histogram showing two Gaussian blobs representing a *Z*
boson background and a Higgs boson signal.

<!-- append example.py -->
```python
# Unbinned data
Z = np.random.normal(90, 10, size=10000)
H = np.random.normal(125, 10, size=1000)

# Manual binning, or reading from TH1F
bins = np.linspace(50, 200, 31)
Z_counts, _ = np.histogram(Z, bins=bins)
H_counts, _ = np.histogram(H, bins=bins)

plt.figure(figsize=(5,4))

# Drawing shapes
plt.hist(bins[:-1], bins=bins, weights=Z_counts,
                    label="$Z$ boson", histtype='stepfilled')
plt.hist(bins[:-1], bins=bins, weights=H_counts, bottom=Z_counts,
                    label="Higgs boson", histtype='stepfilled')

# Styling
plt.xlabel("Mass $m$ / GeV", ha='right', x=0.95)
plt.ylabel("Events / 5 GeV", ha='right', y=0.95)
plt.xlim((bins[0], bins[-1]))
atlasify("Internal", r"$\sqrt{s} = 13\,\mathrm{TeV}$")
plt.tight_layout()
plt.savefig("test_histo.pdf")
```

<!-- append example.py
```python
plt.savefig("test_histo.png", dpi=300)
plt.clf()
```
-->

![ATLAS style
plot](https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/jobs/artifacts/master/raw/test_histo.png?job=doxec)

<!-- console
```
$ python3 example.py
```
-->



