========
atlasify
========


.. toctree::
   :maxdepth: 2
   :hidden:

   Quickstart <self>
   usage
   example
   publications
   module_reference

.. image:: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/badges/master/pipeline.svg
   :target: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/pipelines
   :alt: Pipeline status

.. image:: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/jobs/artifacts/master/raw/pylint.svg?job=pylint
   :alt: Pylint score
   :target: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify

.. image:: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/jobs/artifacts/master/raw/license.svg?job=badges
   :alt: GNU AGPLv3
   :target: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/blob/master/LICENSE

.. image:: https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/jobs/artifacts/master/raw/pypi.svg?job=badges
   :alt: PyPI
   :target: https://pypi.org/project/atlasify/


The Python package `atlasify` applies the ATLAS style to matplotlib plots. This includes

 - Switching to Arial font (not Helvetica since it's not widely available),
 - Adding ticks on all edges,
 - Making ticks to inward,
 - Adding the ***ATLAS*** badge with optional labels (e.g. Internal),
 - Adding a description below the badge, and
 - Moving the ***ATLAS*** badge outside the axes area.

Quickstart
==========

The package will use Helvetica. The
package ships with GPL-licensed Nimbus Sans L as a fallback.

The `atlasify` package can be installed using pip.

.. code::

   pip install atlasify
   # or 
   pip install https://gitlab.sauerburger.com/cern/fsauerbu/atlasify/-/archive/master/atlasify-master.tar.gz

Indices and tables
==================
* `Git repository <https://gitlab.sauerburger.com/cern/fsauerbu/atlasify>`_
* `PyPI repository <https://pypi.org/project/atlasify/>`_
* `Documentation <https://atlasify.readthedocs.org>`_
* :ref:`genindex`
* :ref:`search`
