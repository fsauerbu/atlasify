#!/usr/bin/env python3

import sys
import os
from pylint.lint import Run
import anybadge

args = [
    "--disable=C0103",
    os.path.join(os.path.dirname(__file__), "../setup.py"),
    "atlasify.__init__",
]


results = Run(args, do_exit=False)
status = results.linter.msg_status
score = "%.2f" % results.linter.stats.global_note

thresholds = {8: 'red',
              9: 'orange',
              9.9: 'yellow',
              10: 'green'}
badge = anybadge.Badge('pylint', score, thresholds=thresholds)
badge.write_badge('pylint.svg', overwrite=True)

sys.exit(status)
